#ifndef DAY_H
#define DAY_H

#include <ctime>
#include <string>

// Entry for one day in the calendar
class Day
{
	private:
		// identifier for days
		int number;
		// user's rating of the day
		int rating;
		// user's note for the day
		std::string note;
		// date of last edit
		std::time_t editTime;
		// date of creation
		std::time_t creationDate;
	public:
		// constructor
		Day();
		// sets all member values to default
		void init();

		// getter and setter methods follow
		int getNumber();
		void setNumber(int value);
		int getRating();
		void setRating(int value);
		std::string getNote();
		void setNote(std::string value);
		std::time_t getEditTime();
		void setEditTime(std::time_t value);
		std::time_t getCreationDate();
		void setCreationDate(std::time_t value);
};

#endif
