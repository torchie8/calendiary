// Main menu
#ifndef MENU_H
#define MENU_H

#include "calendar.h"

class Menu
{
	private:
		// menu selection
		char sel;
		Calendar calendar;

	public:
		// print intro text
		void printIntro();
		// day viewing interface
		void viewDay(int dayNum);
		// day editing interface
		void editDay(int dayNum);
		// main menu loop
		void loop();
		// prints current status of loaded calendar
		void printCalendarStatus();
		// selection variable getter
		char getSel();
		// gets a valid entry number from stdin
		int askEntry();
		// calendar getter
		Calendar& getCalendar();
};

#endif
