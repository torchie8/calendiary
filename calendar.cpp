// Contains all day entries
#include "calendar.h"
#include <ctime>
#include "day.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

// load a calendar from file or create a new one
bool Calendar::load(std::string filename)
{
	std::ifstream loadCal;
	loadCal.open(filename);
	if (loadCal.good())
	{
		int lineNum = 0;
		std::string line;
		std::getline(loadCal, line);
		while(loadCal.good())
		{
			addDay();
			int index = 0;
			if (!loadCal.good())
			{
				break;
			}
			std::string rat = "";
			while (line[index] != ' ')
			{
				rat += line[index];
				index++;
			}
			index++;
			days[lineNum].setRating(atoi(rat.c_str()));
			std::string crDate;
			while (line[index] != ' ')
			{
				crDate += line[index];
				index++;
			}
			index++;
			days[lineNum].setCreationDate(atoi(crDate.c_str()));
			std::string edTime;
			while (line[index] != ' ')
			{
				edTime += line[index];
				index++;
			}
			index++;
			days[lineNum].setEditTime(atoi(edTime.c_str()));
			std::string dayNote;
			while (index < line.size())
			{
				dayNote += line[index];
				index++;
			}
			days[lineNum].setNote(dayNote);
			std::getline(loadCal, line);
			if (!loadCal.good())
			{
				break;
			}
			lineNum++;
		}
		std::cout << "\nCalendar loaded from file " << filename << ".\n";
	}
	else
	{
		std::cout << "\nNo calendar file read, creating new calendar...\n";
		days.clear();
	}
	loadCal.close();
	return true;
}

// save calendar to file; returns success/failure
bool Calendar::save(std::string filename)
{
	std::ofstream saveCal;
	saveCal.open(filename);
	if (saveCal.good())
	{
		for (unsigned i = 0; i < days.size(); i++)
		{
			saveCal << days[i].getRating() << ' '
				<< days[i].getCreationDate() << ' '
				<< days[i].getEditTime() << ' '
				<< days[i].getNote() << '\n';
		}
		std::cout << "\nFile " << filename << " saved successfully.\n";
	}
	else
	{
		// error handling...
	}
	saveCal.close();
	return true;
}

// prints current status of calendar
void Calendar::printStatus()
{
	if (days.size() == 0)
	{
		std::cout << "\nThere are no entries.\n";
	}
	else if (days.size() == 1)
	{
		std::time_t t = days[0].getCreationDate();
		char timeStr[50];
		std::strftime(timeStr, 50, "%A %m/%d/%Y", std::localtime(&t));
		std::cout << "\nThere is 1 entry from " << timeStr << ".\n";
	}
	else
	{
		std::time_t t1 = days[0].getCreationDate();
		std::time_t t2 = days.back().getCreationDate();
		char timeStr1[50], timeStr2[50];
		std::strftime(timeStr1, 50, "%A %m/%d/%Y", std::localtime(&t1));
		std::strftime(timeStr2, 50, "%A %m/%d/%Y", std::localtime(&t2));
		std::cout << "\nThere are " << days.size() << " entries from " << timeStr1
			  << " to " << timeStr2 << ".\n";
	}
}

// adds a new day entry
void Calendar::addDay()
{
	Day newDay;
	days.push_back(newDay);
	days.back().setNumber(days.size());
}

// deletes a day entry
void Calendar::deleteDay(int dayNum)
{
	days.erase(days.begin() + dayNum - 1);
}

// getter for days vector
std::vector<Day>& Calendar::getDays()
{
	return days;
}
