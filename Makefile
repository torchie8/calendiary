# Makefile for Calendiary
# created by Matt Black 2016/12/10

PROG = calendiary
win : PROG = calendiary.exe
CC = clang++
win : CC = i686-w64-mingw32-g++
CFLAGS = -std=c++11 -Wall -pedantic
win : CFLAGS = -std=c++11 -Wall -pedantic -static-libgcc -static-libstdc++
OBJS = main.o calendar.o day.o

# Compiling for Linux
linux : $(OBJS)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJS)

# Compiling for Windows
win : $(OBJS)
	$(CC) $(CFLAGS) -o $(PROG) $(OBJS)

main.o : main.cpp menu.h calendar.h day.h
	$(CC) $(CFLAGS) -c main.cpp

calendar.o : calendar.cpp calendar.h day.h
	$(CC) $(CFLAGS) -c calendar.cpp

day.o : day.cpp day.h
	$(CC) $(CFLAGS) -c day.cpp

clean :
	rm -f $(OBJS) calendiary calendiary.exe calendar.dat
