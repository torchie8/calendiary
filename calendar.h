// Contains all day entries
#ifndef CALENDAR_H
#define CALENDAR_H

#include <ctime>
#include "day.h"
#include <string>
#include <vector>

class Calendar
{
	private:
		// stores Day objects
		std::vector<Day> days;
	public:
		// load a calendar from file or create a new one; returns success/failure
		bool load(std::string filename);
		// save calendar to file; returns success/failure
		bool save(std::string filename);
		// prints status
		void printStatus();
		// adds a new day entry
		void addDay();
		// deletes a day entry
		void deleteDay(int dayNum);
		// getter for days vector
		std::vector<Day>& getDays();
};

#endif
