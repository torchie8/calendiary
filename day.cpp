// Entry for one day in the calendar
#include "day.h"
#include <ctime>
#include <string>

// constructor
Day::Day()
{
	Day::setEditTime(std::time(nullptr));
	Day::setRating(-1);
	Day::setCreationDate(std::time(nullptr));
	Day::setNote("");
}

// sets all member variables to default values
void Day::init()
{
	Day::setEditTime(std::time(nullptr));
	Day::setRating(-1);
	Day::setCreationDate(std::time(nullptr));
	Day::setNote("");
}

// getters and setters for member variables follow
int Day::getNumber()
{
	return number;
}

void Day::setNumber(int value)
{
	number = value;
}

int Day::getRating()
{
	return rating;
}

void Day::setRating(int value)
{
	rating = value;
	Day::setEditTime(std::time(nullptr));
}

std::string Day::getNote()
{
	return note;
}

void Day::setNote(std::string value)
{
	note = value;
	Day::setEditTime(std::time(nullptr));
}

std::time_t Day::getEditTime()
{
	return editTime;
}

void Day::setEditTime(std::time_t value)
{
	editTime = value;
}

std::time_t Day::getCreationDate()
{
	return creationDate;
}

void Day::setCreationDate(std::time_t value)
{
	creationDate = value;
}
