// Main/menu code

#include "calendar.h"
#include "day.h"
#include "menu.h"
#include <vector>
#include <iostream>
#include <string>
#include <ctime>

#define DAYS_SIZE calendar.getDays().size()

int main()
{
	Menu menu;
	menu.printIntro();
	menu.getCalendar().load("calendar.dat");
	menu.loop();
	return 0;
}

// prints a short text intro
void Menu::printIntro()
{
	std::cout << "\nCalendiary\n"
		  << "\nCommands: 'n' to create a [n]ew entry\n"
		  << "          'e' to [e]dit an existing entry\n"
		  << "          'v' to [v]iew an entry\n"
		  << "          'd' to [d]elete an entry\n"
		  << "          'a' to list [a]ll entries\n"
		  << "          'l' to [l]oad a calendar file\n"
		  << "          's' to [s]ave a calendar file\n"
		  << "          'h' to show this [h]elp sheet\n"
		  << "          'q' to [q]uit\n";
}

// getter for selection variable
char Menu::getSel()
{
	return sel;
}

// gets a valid entry number from stdin
int Menu::askEntry()
{
	int entryNum;
	do
	{
		std::cout << "\nSelect an entry number from 1 to " << DAYS_SIZE << ".\n# ";
		std::cin >> entryNum;
		if (entryNum < 1 || entryNum > DAYS_SIZE)
		{
			std::cout << "That entry does not exist.\n";
		}
	}
	// TODO: add exception handling
	while (entryNum < 1 || entryNum > DAYS_SIZE);
	return entryNum;
}

// getter for global calendar
Calendar& Menu::getCalendar()
{
	return calendar;
}

// print an entry's information
void Menu::viewDay(int dayNum)
{
	char creationStr[50];
	std::time_t creationTime = calendar.getDays()[dayNum-1].getCreationDate();
	std::strftime(creationStr, 50, "%A %m/%d/%Y, %H:%M", std::localtime(&creationTime));
	char editTimeStr[50];
	std::time_t editDate = calendar.getDays()[dayNum-1].getEditTime();
	std::strftime(editTimeStr, 50, "%A %m/%d/%Y, %H:%M", std::localtime(&editDate));
	std::cout << "\nDay number " << calendar.getDays()[dayNum-1].getNumber() << ".\n\""
		  << calendar.getDays()[dayNum-1].getNote() << "\""
		  << "\nDay Rating:  " << calendar.getDays()[dayNum-1].getRating() << "/10"
		  << "\nDate:        " << creationStr
		  << "\nLast edited: " << editTimeStr << std::endl;
}

// interface for editing entries
void Menu::editDay(int dayNum)
{
	std::cout << "\nEditing entry number " << dayNum << "."
		  << "\nPlease enter today's note. How has your day been?\n> ";
	std::string tempNote;
	std::cin.ignore();
	std::getline(std::cin, tempNote);
	calendar.getDays()[dayNum-1].setNote(tempNote);
	int tempRating;
	do
	{
		std::cout << "\nRate your day on a scale of 1 to 10: ";
		std::cin.clear();
		std::cin >> tempRating;
		if (tempRating < 1)
		{
			std::cout << "\nI am sorry that your day was that bad. But please, enter a rating of 1-10 regardless.";
		}
		else if (tempRating > 10)
		{
			std::cout << "\nGlad to hear you have had a great day! But please, stick to the 1-10 rating scale!";
		}
	}
	while (tempRating > 10 || tempRating < 1);
	calendar.getDays()[dayNum-1].setRating(tempRating);
	calendar.getDays()[dayNum-1].setEditTime(std::time(nullptr));
	std::cout << "Entry saved. Thank you!\n";
}

// main program loop
void Menu::loop()
{
	while (sel != 'q')
	{
		calendar.printStatus();
		std::cout << "> ";
		std::cin >> sel;
		std::string file;
		switch (sel)
		{
			// add new entry
			case 'n': case 'N':
				calendar.addDay();
				Menu::editDay(DAYS_SIZE);
				break;
			// edit existing entry
			case 'e': case 'E':
				if (DAYS_SIZE > 0)
				{
					Menu::editDay(askEntry());
				}
				else
				{
					std::cout << "\nCannot edit - there are no entries yet.\n";
				}
				break;
			case 'v': case 'V':
				if (DAYS_SIZE > 0)
				{
					Menu::viewDay(askEntry());
				}
				else
				{
					std::cout << "\nCannot view - there are no entries yet.\n";
				}
				break;
			case 'd': case 'D':
				if (DAYS_SIZE > 0)
				{
					calendar.deleteDay(askEntry());
				}
				else
				{
					std::cout <<"\nCannot delete - there are no entries yet.\n";
				}
				break;
			case 'l': case 'L':
				std::cout << "\nEnter filename to load from.\n> ";
				std::cin >> file;
				calendar.load(file);
				break;
			case 's': case 'S':
				calendar.save("calendar.dat");
				break;
			case 'h': case 'H':
				printIntro();
				break;
			default:
				if (sel != 'q')
				{
					std::cout << "Unrecognized command.\n";
				}
				break;
		}
	}
}
